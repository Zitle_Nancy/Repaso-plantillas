(function() {
	var listaPaisajes =
	[
		{
			"foto": "img/1.jpg",
			"tema": "naturaleza"
		},
		{
			"foto": "img/2.jpg",
			"tema": "paisaje"
		}
	];
	var plantilla = 
		'<div class="container">'+
				'<div class="row">'+
					'<div class="col-xs-12 col-md-6 ">'+
						'<div class="thumbnail">'+
							'<img src="__foto__" alt="fotos">'+
						'</div>'+
						'<div class="caption text-center">'+
							'<h3><strong>__tema__</strong></h3>'+
						'</div>'+
					'</div>'+
				'</div>'+
		'</div>';
	var cargarPagina = function(){
		$('#btn-crear').click(imprimirPlantilla);
		// imprimirPlantilla(listaPaisajes);
		// console.log(listaPaisajes);
	};
	var imprimirPlantilla = function(){
		var listas = listaPaisajes;
		console.log(listas);
		var plantillaMostrar = '';
		listas.forEach(function(paisaje){
			plantillaMostrar += plantilla.replace('__foto__',paisaje.foto)
										 .replace('__tema__',paisaje.tema);
		});

		$('#muro').append(plantillaMostrar);

	}

	$(document).ready(cargarPagina);
})();